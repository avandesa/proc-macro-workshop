extern crate proc_macro;

use {
    proc_macro::TokenStream,
    proc_macro2::TokenStream as TokenStream2,
    quote::{format_ident, quote},
    syn::{parse_macro_input, spanned::Spanned, DeriveInput, Error, Result},
};

#[proc_macro_derive(Builder, attributes(builder))]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    my_derive(input)
        .unwrap_or_else(|err| err.to_compile_error())
        .into()
}

fn my_derive(input: DeriveInput) -> Result<TokenStream2> {
    let struct_ident = &input.ident;
    let struct_data = get_struct_data(&input)?;
    let struct_fields = get_named_fields(&struct_data)?;

    let builder_ident = format_ident!("{}Builder", struct_ident);
    let builder_fields = make_builder_fields(&struct_fields)?;
    let builder_fields_none = make_builder_fields_init(&struct_fields)?;
    let builder_fns = make_builder_methods(&struct_fields)?;
    let builder_build_fn = make_builder_build_fn(&struct_ident, &struct_fields)?;

    let expanded = quote! {
        impl #struct_ident {
            pub fn builder() -> #builder_ident {
                #builder_ident {
                    #(#builder_fields_none)*
                }
            }
        }

        pub struct #builder_ident {
            #(#builder_fields)*
        }

        impl #builder_ident {
            #(#builder_fns)*

            #builder_build_fn
        }
    };

    Ok(expanded)
}

fn get_struct_data(input: &DeriveInput) -> Result<syn::DataStruct> {
    if let syn::Data::Struct(data) = &input.data {
        Ok(data.clone())
    } else {
        Err(Error::new_spanned(input, "only structs are supported"))
    }
}

fn get_named_fields(data: &syn::DataStruct) -> Result<Vec<syn::Field>> {
    if let syn::Fields::Named(fields) = &data.fields {
        Ok(fields.named.iter().map(|f| f.clone()).collect())
    } else {
        Err(Error::new_spanned(
            &data.struct_token,
            "only structs with named fields are supported",
        ))
    }
}

fn field_is_option(field: &syn::Field) -> Option<syn::Type> {
    let ty = &field.ty;
    if let syn::Type::Path(type_path) = ty {
        let path = &type_path.path;

        let seg = &path.segments[0];
        if seg.ident == "Option" {
            if let syn::PathArguments::AngleBracketed(type_args) = &path.segments[0].arguments {
                if let syn::GenericArgument::Type(actual_type) = &type_args.args[0] {
                    return Some(actual_type.clone());
                }
            }
        }
    }

    None
}

fn field_is_vec(field: &syn::Field) -> Option<syn::Type> {
    let ty = &field.ty;
    if let syn::Type::Path(type_path) = ty {
        let path = &type_path.path;

        let seg = &path.segments[0];
        if seg.ident == "Vec" {
            if let syn::PathArguments::AngleBracketed(type_args) = &path.segments[0].arguments {
                if let syn::GenericArgument::Type(actual_type) = &type_args.args[0] {
                    return Some(actual_type.clone());
                }
            }
        }
    }

    None
}

fn field_is_repeated(field: &syn::Field) -> Result<Option<(syn::Ident, syn::Type)>> {
    let attrs = &field.attrs;
    if let Some(builder_attr) = attrs.iter().find(|attr| attr.path.is_ident("builder")) {
        if let Ok(syn::Meta::List(meta)) = &builder_attr.parse_meta() {
            let nested_meta = &meta.nested[0];

            if let syn::NestedMeta::Meta(syn::Meta::NameValue(nv)) = nested_meta {
                if !nv.path.is_ident("each") {
                    return Err(Error::new(
                        builder_attr
                            .path
                            .span()
                            .join(builder_attr.tokens.span())
                            .unwrap(),
                        "expected `builder(each = \"...\")`",
                    ));
                }

                if let syn::Lit::Str(lit_str) = &nv.lit {
                    let each = syn::Ident::new(&lit_str.value(), proc_macro2::Span::call_site());
                    if let Some(inner_type) = field_is_vec(field) {
                        return Ok(Some((each, inner_type)));
                    }
                }
            }
        }
    }

    Ok(None)
}

fn make_builder_fields(fields: &[syn::Field]) -> Result<Vec<TokenStream2>> {
    fields
        .iter()
        .map(|field| {
            let name = field.ident.clone().unwrap();
            let ty = field.ty.clone();

            let _ = field_is_repeated(&field);

            if let Some(inner) = field_is_option(&field) {
                Ok(quote! {
                    #name: ::std::option::Option<#inner>,
                })
            } else if let Some((_, inner)) = field_is_repeated(&field)? {
                Ok(quote! {
                    #name: ::std::vec::Vec<#inner>,
                })
            } else {
                Ok(quote! {
                    #name: ::std::option::Option<#ty>,
                })
            }
        })
        .collect()
}

fn make_builder_fields_init(fields: &[syn::Field]) -> Result<Vec<TokenStream2>> {
    fields
        .iter()
        .map(|field| {
            let name = field.ident.clone().unwrap();

            if let Some((_, inner)) = field_is_repeated(&field)? {
                Ok(quote! {
                    #name: ::std::vec::Vec::<#inner>::new(),
                })
            } else {
                Ok(quote! {
                    #name: ::std::option::Option::None,
                })
            }
        })
        .collect::<Result<Vec<TokenStream2>>>()
}

fn make_builder_methods(fields: &[syn::Field]) -> Result<Vec<TokenStream2>> {
    fields
        .iter()
        .map(|field| {
            let name = field.ident.clone().unwrap();
            let ty = field.ty.clone();

            if let Some(inner) = field_is_option(&field) {
                Ok(quote! {
                    fn #name(&mut self, #name: #inner) -> &mut Self {
                        self.#name = ::std::option::Option::Some(#name);
                        self
                    }
                })
            } else if let Some((each, inner)) = field_is_repeated(&field)? {
                Ok(quote! {
                    fn #each(&mut self, #each: #inner) -> &mut Self {
                        self.#name.push(#each);
                        self
                    }
                })
            } else {
                Ok(quote! {
                    fn #name(&mut self, #name: #ty) -> &mut Self {
                        self.#name = ::std::option::Option::Some(#name);
                        self
                    }
                })
            }
        })
        .collect()
}

fn make_builder_build_fn(struct_ident: &syn::Ident, fields: &[syn::Field]) -> Result<TokenStream2> {
    let inits = make_builder_build_fn_fields(fields)?;

    Ok(quote! {
        pub fn build(
            &mut self
        ) -> ::std::result::Result<#struct_ident, ::std::boxed::Box<dyn std::error::Error>> {
            std::result::Result::Ok(#struct_ident {
                #(#inits),*
            })
        }
    })
}

fn make_builder_build_fn_fields(fields: &[syn::Field]) -> Result<Vec<TokenStream2>> {
    fields
        .iter()
        .map(|field| {
            let name = field.ident.clone().unwrap();

            if field_is_option(&field).is_some() || field_is_repeated(&field)?.is_some() {
                Ok(quote! {
                    #name: self.#name.clone()
                })
            } else {
                Ok(quote! {
                    #name: self.#name.clone().expect("All fields must be initialized")
                })
            }
        })
        .collect()
}
